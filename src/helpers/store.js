import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers';
const enhance = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const loggerMiddleware = createLogger();

export const store = createStore(
    rootReducer,
    enhance(applyMiddleware(
        thunkMiddleware,
        loggerMiddleware
    ))
);