export function setHostURL() {
  const url =  window.location.href;

  if(url.startsWith('http://localhost:3000')) {
      return 'http://localhost:4000';
  } else {
      return 'https://www.bloggio.org';
  }
}

export function getToken() {
     // return authorization header with jwt token
     return JSON.parse(localStorage.getItem('token'));

    
}