import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {userActions} from '../actions/user.actions';


class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            nick: '',
            password: '',
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { nick, password } = this.state;
        const { dispatch } = this.props;
        if (nick && password) {
            dispatch(userActions.login(nick, password));
        }
    }

    render() {
        const { nick, password, submitted } = this.state;
        return (
            <div className="row">
                <div className="col-eight tab-full">
                <h2 className="add-bottom">Login</h2>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !nick ? ' has-error' : '')}>
                        <label htmlFor="nick">Username</label>
                        <input type="text" placeholder="Your username" className="full-width" name="nick" value={nick} onChange={this.handleChange} />
                        {submitted && !nick &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !password ? ' has-error' : '')}>
                        <label htmlFor="password">Password</label>
                        <input type="password" placeholder="Your password" className="full-width" name="password" value={password} onChange={this.handleChange} />
                        {submitted && !password &&
                            <div className="help-block">Password is required</div>
                        }
                    </div>
                    
                        <button type="submit" className="btn--primary full-width">Login</button>
                     
                        <Link className="btn btn--primary full-width" to="/register" >Register</Link>

                    
                </form>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn } = state.authentication;
    return {
        loggingIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 


