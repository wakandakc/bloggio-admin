import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {headerEvents} from './events';



const Header = props => {
 
    const isLoggedIn = props.loggedIn;
   
          return ( 
           
            <section className="s-pageheader bloggio-header-spage">
        
                <header className="header">
                    <div className="header__content row">
                        <div className="header__logo">
                            <a className="logo" href="https://www.bloggio.org">
                                <h1>Bloggio</h1>
                            </a>
                        </div>                       
                        

              {isLoggedIn &&  <React.Fragment>
          

                <a className="header__search-trigger" href="#0" onClick={headerEvents.clSearch}></a>

                <div className="header__search">

                    <form role="search" method="get" className="header__search-form" action="#">
                        <label>
                            <span className="hide-content">Search for:</span>
                            <input type="search" className="search-field" placeholder="Type Keywords" name="s" title="Search for:" autoComplete="off"/>
                        </label>
                        <input type="submit" className="search-submit" value="Search"/>
                    </form>
        
                    <a href="#0" title="Close Search" className="header__overlay-close">Close</a>

                </div>  


                <a className="header__toggle-menu" href="#0" title="Menu" onClick={headerEvents.clMobileMenu}><span>Menu</span></a>
                <nav className="header__nav-wrap">

                    <h2 className="header__nav-heading h6">Site Navigation</h2>


                    <ul className="header__nav">
                        <li className="current"><a href="https://www.bloggio.org" title="">Home</a></li>
                        <li className="has-children">
                            <a href="#0" title="">Articles</a>
                            <ul className="sub-menu">
                            <li><Link to= {'/newPost'} className="nav-link" title="UpdateSettings">New post</Link></li>
                            <li><Link to={'/posts'} className="nav-link" title="UpdateSettings">My posts</Link></li>
                            </ul>
                        </li>
                        <li className="has-children">
                            <a href="#0" title="">Others</a>
                            <ul className="sub-menu">
                            <li><a href="#">Bookmarks</a></li>
                            <li><a href="#">Highligtheds</a></li>
                            </ul>
                        </li>
                        <li className="has-children">
                            <a href="#0" title="">Notifications</a>
                            <ul className="sub-menu">
                            <li><a href="#">New Publications</a></li>
                            <li><a href="#">New Mentions</a></li>
                            </ul>
                        </li>
                        <li className="has-children">
                            <a href="#0" title="">Profile</a>
                            <ul className="sub-menu">
                            <li><a href="#">Resume</a></li>
                            <li><Link to= {'/updateSettings'} className="nav-link" title="UpdateSettings">Settings</Link></li>
                            <li>  <Link to= {'/login'} className="nav-link" title="Logout">Logout</Link></li>
                            </ul>
                        </li>
                       
                    </ul> 

                    <a href="#0" title="Close Menu" className="header__overlay-close close-mobile-menu">Close</a>

                </nav> 

       
                </React.Fragment>

              }
            
              </div> 
        </header> 
            </section> 
        
           );
     }

 

     function mapStateToProps(state) {
        const { authentication } = state;
        const { loggedIn } = authentication;
        return {
            
           loggedIn
        };
    }
      
    export default connect(mapStateToProps)(Header);
      