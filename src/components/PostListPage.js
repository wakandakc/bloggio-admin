import React from 'react';
import { connect } from 'react-redux';
import { postActions } from '../actions/post.actions';




class PostListPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            posts: props.posts
        };

        this.handleGetAll = this.handleGetAll.bind(this);
        this.viewDetail = this.viewDetail.bind(this);

        this.handleGetAll();

    }

    viewDetail(post) {
        const { dispatch } = this.props;
        dispatch(postActions.detail(post))
    }

    handleGetAll() {
        const { dispatch } = this.props;
        dispatch(postActions.getAll());
    }

    componentWillUpdate(e) {
        const { posts } = this.state;
        if ( e.posts !== posts ) {
            this.setState({
                posts: e.posts
            });
        }
    }

    render() {
        const { posts } = this.state;
        return(
            <div className="row add-bottom">

                <div className="col-twelve">

                    <h3>My Posts</h3>

                    <div className="table-responsive">

                        <table>
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>State</th>
                                    <th>Date insert</th>
                                </tr>
                                </thead>
                                <tbody>
                                {posts && posts.length > 0 && posts.map(post => (
                                    <tr>
                                        <td><a onClick={() => this.viewDetail(post)}>{post.title}</a></td>
                                        <td>{post.state}</td>
                                        <td>{new Date(post.date_insert).toLocaleDateString()}</td>
                                    </tr>
                                ))}
                                {(!posts || posts.length === 0) && (
                                    <tr>
                                        <td colSpan="3">No posts found...</td>
                                    </tr>
                                )}
                                </tbody>
                        </table>

                    </div>

                </div>
            
            </div> 
    
        );
    }
}

function mapStateToProps(state) {
    const { posts, post } = state.postReducer;
    return {
        posts,
        post
    };
}

const connectedPostListPage = connect(mapStateToProps)(PostListPage);
export { connectedPostListPage as PostListPage };