import $ from 'jquery';

 /* Smooth Scrolling
    * ------------------------------------------------------ */
function clSmoothScroll() {
           
    var cfg = {
        scrollDuration : 800, // smoothscroll duration
        mailChimpURL   : 'https://facebook.us8.list-manage.com/subscribe/post?u=cdb7b577e41181934ed6a6a44&amp;id=e6957d85dc'   // mailchimp url
    }

    $('.smoothscroll').on('click', function (e) {
        var target = this.hash,
        $target    = $(target);
        
            e.preventDefault();
            e.stopPropagation();

        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, cfg.scrollDuration, 'swing').promise().done(function () {

            // check if menu is open
            if ($('body').hasClass('menu-is-open')) {
                $('.header-menu-toggle').trigger('click');
            }

            window.location.hash = target;
        });
    });

};


function clBackToTop() {
        
    var pxShow      = 200,
        goTopButton = $(".go-top")

    // Show or hide the button
    if ($(window).scrollTop() >= pxShow) goTopButton.addClass('link-is-visible');

    $(window).on('scroll', function() {
        if ($(window).scrollTop() >= pxShow) {
            if(!goTopButton.hasClass('link-is-visible')) goTopButton.addClass('link-is-visible')
        } else {
            goTopButton.removeClass('link-is-visible')
        }
    });
};

export const footerEvents = {
    clSmoothScroll,
    clBackToTop
}