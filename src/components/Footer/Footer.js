import React from 'react'
import {footerEvents} from './events';

class Footer extends React.Component{
  componentDidMount() {
      footerEvents.clBackToTop();
  }
    
    render () {

        return(
        <footer className="s-footer">
            <div className="s-footer__bottom">
            <div className="row">
                <div className="col-full">
                    <div className="s-footer__copyright">
                        <span>© Copyright Bloggio 2019</span> 
                        <span>Site by Wakanda Team</span>
                    </div>

                    <div className="go-top">
                        <a className="smoothscroll" title="Back to Top" href="#top" onClick={footerEvents.clSmoothScroll}></a>
                    </div>
                </div>
            </div>
        </div> 
        </footer>
        );
}

}

export default Footer;
