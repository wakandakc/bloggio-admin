import React from 'react';
import { connect } from 'react-redux';
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { postActions } from '../actions/post.actions';




class PostPage extends React.Component {

    constructor(props) {
        super(props);

        console.log('props', props)

        if (props.post) {
            this.state = {
                post: props.post,
                submited: false
            }
        } else {
            this.state = {
                post: {
                    title: undefined,
                    path: undefined,
                    state: undefined,
                    intro_text: undefined,
                    body_text: undefined
                },
                submitted: false
            };
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.textChange = this.textChange.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        const { post } = this.state;
        this.setState({
            post: {
                ...post,
                [name]: value
            }
        });
    }

    textChange(value) {
        const { post } = this.state;
        this.setState({
            post: {
                ...post,
                body_text: value
            }
        });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true })
        const { post } = this.state;
        const {dispatch } = this.props;
        if (
            post.title &&
            post.path &&
            post.state &&
            post.intro_text &&
            post.body_text
        ) {
            dispatch(postActions.submit(post));

        }
    }

    handleDelete(e) {
        e.preventDefault();
        const { post } = this.state;
        const { dispatch } = this.props;
        dispatch(postActions._delete(post));
        
    }

    render() {
        const { submitted, post } = this.state;
        return(
            <div className="row">
                <h2 className="add-bottom">New Post</h2>
                <div className="col tab-full">
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !post.title ? ' has-error' : '')}>
                        <label htmlFor="title">Title</label>
                        <input type="text" placeholder="Post title" className="full-width" name="title" value={post && post.title} onChange={this.handleChange} />
                        {submitted && !post.title &&
                            <div className="help-block">Title is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !post.path ? ' has-error' : '')}>
                        <label htmlFor="url">Url</label>
                        <input pattern="[\w-]+(?:[\w-]+)+$" type="text" className="full-width" placeholder="Post Url" name="path" value={post && post.path} onChange={this.handleChange} />
                        {submitted && !post.path &&
                            <div className="help-block">Url is required</div>
                        }
                    </div>
                    
                    <div className={'form-group' + (submitted && !post.state ? ' has-error' : '')}>
                        <label htmlFor="state">State</label>
                        <div className="cl-custom-select">
                            <select className="full-width" name="state" value={post && post.state} onChange={this.handleChange} >
                                <option></option>
                                <option value="DRAFT">Draft</option>
                                <option value="PUBLISHED">Published</option>
                            </select>
                        </div>
                        {submitted && !post.state &&
                            <div className="help-block">State is required</div>
                        }
                    </div>

                    <div className={'form-group' + (submitted && !post.intro_text ? ' has-error' : '')}>
                        <label htmlFor="introText">Intro</label>
                        <textarea className="full-width" placeholder="Intro text" name="intro_text" value={post && post.intro_text} onChange={this.handleChange} ></textarea>
                    </div>
                        
                    <div className={'form-group' + (submitted && !post.body_text ? ' has-error' : '')}>
                        <label htmlFor="introText">Body</label>
                        <CKEditor
                            editor={ ClassicEditor }
                            onChange={ ( event, editor ) => {
                                const data = editor.getData();
                                this.textChange(data);
                            } }
                            onInit={ editor => {
                                if (post.body_text) {
                                    editor.setData(post.body_text)
                                }
                            } }
                            config={ {toolbar: [ 'heading', '|', 'undo', 'redo', '|', 'bold', 'italic', '|', 'link', 'bulletedList', 'numberedList', 'blockQuote' ] }
                        }
                        />
                    </div>

                    <div className="form-group">
                        <button className="btn btn--primary full-width">Send post</button>
                    </div>
                    {post._id && 
                        <div className="form-group">
                            <button className="btn btn--medium full-width" onClick={this.handleDelete}>Delete</button>
                        </div>
                    }
                </form>
            </div>
            </div>
    
        );
    }
}

function mapStateToProps(state) {
    const { post } = state.postReducer;
    return {
        post
    };
}

const connectedPostPage = connect(mapStateToProps)(PostPage);
export { connectedPostPage as PostPage };