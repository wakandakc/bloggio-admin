import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import { userActions } from '../actions/user.actions';

import small from '../images/thumbs/about-500.jpg';
import medium from '../images/thumbs/about-1000.jpg';
import large from '../images/thumbs/about-2000.jpg';



class HomePage extends React.Component {


    componentDidMount() {
       // this.props.dispatch(userActions.getAll());
    }

    handleDeleteUser(id) {
        return (e) => this.props.dispatch(userActions.delete(id));
    }

    render() {
        const { user} = this.props;
 
        return(
            <div className="row">

            <div className="s-content__header col-full">
                <h1 className="s-content__header-title">
                    Hi {user.firstName} {user.lastName}!
                </h1>
            </div> 

            <div className="s-content__media col-full">
                <div className="s-content__post-thumb">
                <img src={medium} 
                    srcSet={`${small} 500w, ${medium} 1000w, ${large} 2000w`}
                    sizes="(max-width: 2000px) 100vw, 2000px" alt="" / >
                </div>
            </div> 

            <div className="col-full s-content__main">

                <p className="lead">You're logged in with React!!</p>
                

                <p>
                    <Link className="btn btn--primary button.btn--medium" to="/login">Logout</Link>
                </p>

                


            </div> 

        </div> 
    
        );
    }
}

function mapStateToProps(state) {
    const { users, authentication } = state;
    const { user } = authentication;
    return {
        user,
        users
    };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };