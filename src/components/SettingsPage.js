import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {userActions} from '../actions/user.actions';

class SettingsPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user,
            submitted: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleSubmit(event) {
        event.preventDefault();

        this.setState({ submitted: true });
        const { user } = this.state;
        const { dispatch } = this.props;
        if (user.firstName && user.lastName && user.nick && user.email) {
           dispatch(userActions.updateSettings(user));
        }
    }

    render() {
        const { user, submitted } = this.state;
        return (
            <div className="row">
                <div className="col-eight tab-full">
                <h2 className="add-bottom">Update Your Settings</h2>
                <form name="form" onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !user.firstName ? ' has-error' : '')}>
                        <label htmlFor="firstName">First Name</label>
                        <input type="text" placeholder="Your Name" className="full-width" name="firstName" value={user.firstName} onChange={this.handleChange} />
                        {submitted && !user.firstName &&
                            <div className="help-block">First Name is required</div>
                        }
                    </div>
                    <div className={'form-group' + (submitted && !user.lastName ? ' has-error' : '')}>
                        <label htmlFor="lastName">Last Name</label>
                        <input type="text" className="full-width" placeholder="Your last name" name="lastName" value={user.lastName} onChange={this.handleChange} />
                        {submitted && !user.lastName &&
                            <div className="help-block">Last Name is required</div>
                        }
                    </div>
                    
                    <div className={'form-group' + (submitted && !user.email ? ' has-error' : '')}>
                        <label htmlFor="email">Email</label>
                        <input type="email" className="full-width" placeholder="Your email address" name="email" value={user.email} onChange={this.handleChange} />
                        {submitted && !user.email &&
                            <div className="help-block">Email is required</div>
                        }
                    </div>

                    <div className={'form-group' + (submitted && !user.nick ? ' has-error' : '')}>
                        <label htmlFor="nick">Username</label>
                        <input type="text" className="full-width" placeholder="Your username" name="nick" value={user.nick} onChange={this.handleChange} />
                        {submitted && !user.nick &&
                            <div className="help-block">Username is required</div>
                        }
                    </div>
                    <div className='form-group'>
                        <label htmlFor="password">Password</label>
                        <input disabled="{true}" type="password" className="full-width" placeholder="Your password" name="password" value={user.password} onChange={this.handleChange} />
                       
                    </div>
                    <div className="form-group">
                        <button className="btn btn--primary full-width">Update Settings</button>
                        
                        <Link to="/" className="btn btn--primary full-width">Cancel</Link>
                    </div>
                </form>
            </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const {authentication}= state;
    const { user } = authentication;
        return {    
           user
        };
}

const connectedSettingsPage = connect(mapStateToProps)(SettingsPage);
export { connectedSettingsPage as SettingsPage };