import React, { Component } from 'react';
import {Router,Route, Switch}  from 'react-router-dom';
import { HomePage } from './components/HomePage';
import { LoginPage } from './components/LoginPage';
import { RegisterPage } from './components/RegisterPage';
import {PrivateRoute} from './components/PrivateRoute';
import { history } from './helpers/history';
import { connect } from 'react-redux';
import { alertActions } from './actions/alert.actions';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import {SettingsPage} from './components/SettingsPage';
import {PostPage} from './components/PostPage';
import { PostListPage } from './components/PostListPage';


class App extends Component {

  constructor(props) {
      super(props);

      const { dispatch } = this.props;
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
  }

  handleClickShowAlert() {

    let {alert} = this.props;
    alert.message='';
      this.setState({
        alert
      });
  }


 

  render() {
    const { alert } = this.props;
    
    return (
        
                        <Router history= {history}>
                        <React.Fragment>
                            <Header/>
                          <section className="s-content s-content--narrow">

                            <div className="row">
                            {alert.message &&
                            <div className={`alert-box alert-box--${alert.type}`}  >
                                <p>{alert.message}</p>
                                <i className="fa fa-times alert-box__close" onClick={this.handleClickShowAlert.bind(this)}></i>
                            </div>
                            }
                                <Switch>
                                    <PrivateRoute exact path="/" component={HomePage} />
                                    <Route path="/login" component={LoginPage} />
                                    <Route path="/register" component={RegisterPage} />
                                    <PrivateRoute path="/updateSettings" component={SettingsPage} />
                                    <PrivateRoute path="/newPost" component={PostPage} />
                                    <PrivateRoute path="/posts" component={PostListPage} />
                                </Switch>
                             </div>
                            </section>
                            <Footer/>
                            </React.Fragment>
                            </Router>
                   
        

    );
  }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}


export default connect(
    mapStateToProps)(App);