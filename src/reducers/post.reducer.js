import { postConstants } from '../constants/post.constants';

const initialState =  { posts: [], post: {} };

export function postReducer(state = initialState, action) {
  switch (action.type) {
    case postConstants.SUBMIT_REQUEST:
      return {};
    case postConstants.SUBMIT_SUCCESS:
      return {};
    case postConstants.SUBMIT_FALIURE:
      return {};
    case postConstants.GET_POSTS_REQUEST:
      return {};
    case postConstants.GET_POSTS_SUCCESS:
      return {
        posts: action.posts
      };
    case postConstants.GET_POSTS_FALIURE:
      return {};
    case postConstants.GO_TO_DETAIL:
      return {
        post: action.post
      };
    default:
      return state
  }
}