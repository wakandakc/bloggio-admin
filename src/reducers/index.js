import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { alert } from './alert.reducer';
import { postReducer } from './post.reducer';


const applicationreducer = combineReducers({
  authentication,
  registration,
  alert,
  postReducer
});

const rootReducer = (state,action) => {
  return applicationreducer(state,action)
}

export default rootReducer;