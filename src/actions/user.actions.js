import { history } from "../helpers/history";
import { alertActions } from "./alert.actions";
import { userConstants } from "../constants/user.constants";
import { userService } from "../services/user.service";
import { setHostURL, getToken} from '../helpers/utils';

export const userActions = {
  login,
  logout,
  register,
  updateSettings,
  delete: _delete
};

function login(nick, password) {
  let user = JSON.stringify({ nick, password });
  return dispatch => {

    dispatch(request(user));

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ nick, password })
    };


    fetch(
       
      setHostURL()+"/apiv1/users/authenticate",
      requestOptions
    )
      .then(response => {
        response.json().then(data => {
          if (data.user) {
            localStorage.setItem("token", JSON.stringify(data.token));
            localStorage.setItem("user", JSON.stringify(data.user));
            console.log('url', setHostURL());
            setTimeout(() => {

              window.location.href = setHostURL()+`/loginsso?token=${data.token}`;
              dispatch(success(data.user));
            }, 5000)
          } else {
            dispatch(failure(data.error.toString()));
            dispatch(alertActions.error(data.error.toString()));
          }
        });
      })
      .catch(err => {
        dispatch(failure(err.toString()));
        dispatch(alertActions.error(err.toString()));
      });
  };
  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

function logout() {

  let user = localStorage.getItem('user');
  localStorage.removeItem('user');
  localStorage.removeItem('token');
  if (user) {
  window.location.href =setHostURL() + `/logoutsso`;
  }
  return { type: userConstants.LOGOUT };
}

function register(user) {
  return dispatch => {
    dispatch(request(user));

    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(user)
    };

    fetch(
        setHostURL() + "/apiv1/users/",
        requestOptions
      )
      .then(response => {
        response.json().then(data => {
            dispatch(success(data.user));
            history.push('/login');
            dispatch(alertActions.success('Registration successful'));
        });
      })
      .catch(err => {
        dispatch(failure(err.toString()));
        dispatch(alertActions.error(err.toString()));
      });
  }

  function request(user) {
    return { type: userConstants.REGISTER_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.REGISTER_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.REGISTER_FAILURE, error };
  }
}


function updateSettings(user) {
    return dispatch => {
      dispatch(request(user));

      const requestOptions = {headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': getToken()
        },
        method: 'put',
        body: JSON.stringify(user)
        }
      fetch(
          setHostURL() + "/apiv1/users/",
          requestOptions
        )
        .then(response => {
          response.json().then(data => {
              localStorage.setItem("user", JSON.stringify(data.user));
              dispatch(success(data.user));
              history.push('/');
              dispatch(alertActions.success('Your settings have been updated successfully'));
          });
        })
        .catch(err => {
          dispatch(failure(err.toString()));
          dispatch(alertActions.error(err.toString()));
        });
    }
  
    function request(user) {
      return { type: userConstants.UPDATE_REQUEST, user };
    }
    function success(user) {
      return { type: userConstants.UPDATE_SUCCESS, user };
    }
    function failure(error) {
      return { type: userConstants.UPDATE_FAILURE, error };
    }
  }

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  return dispatch => {
    dispatch(request(id));

    userService
      .delete(id)
      .then(
        user => dispatch(success(id)),
        error => dispatch(failure(id, error.toString()))
      );
  };

  function request(id) {
    return { type: userConstants.DELETE_REQUEST, id };
  }
  function success(id) {
    return { type: userConstants.DELETE_SUCCESS, id };
  }
  function failure(id, error) {
    return { type: userConstants.DELETE_FAILURE, id, error };
  }
}
