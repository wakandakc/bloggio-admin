import { history } from "../helpers/history";
import { alertActions } from "./alert.actions";
import { postConstants } from "../constants/post.constants";
import { setHostURL, getToken } from '../helpers/utils';

export const postActions = {
  submit,
  getAll,
  detail,
  _delete
};

function submit({ title, path, state, intro_text, body_text, _id }) {
  const id = _id;
  let post = { title, path, state, intro_text, body_text };
  if (id) {
    post.id = id;
  }
  return dispatch => {

    dispatch(request(post));

    let requestOptions = {}

    if (id) {

      requestOptions = {
        method: "PUT",
        headers: { 
          "Content-Type": "application/json",
          'x-access-token': getToken()
        },
        body: JSON.stringify(post)
      };

    } else {

      requestOptions = {
        method: "POST",
        headers: { 
          "Content-Type": "application/json",
          'x-access-token': getToken()
        },
        body: JSON.stringify(post)
      };

    }

    fetch(
       
      setHostURL()+"/apiv1/posts/",
      requestOptions

    )
      .then(response => {
        response.json().then(
          data => {
          if (data.success) {
            dispatch(success(data));
            history.push('/');
            if (id) {
              dispatch(alertActions.success('Your post have been updated successfully'));
            } else {
              dispatch(alertActions.success('Your post have been published successfully'));
            }
            window.open( setHostURL() + '/' + data.result.url, '_blank');
          } else {
            dispatch(failure(data.error.toString()));
            dispatch(alertActions.error(data.error.toString()));
          }
        });
      })
      .catch(err => {
        dispatch(failure(err.toString()));
        dispatch(alertActions.error(err.toString()));
      });
  };
  function request(post) {
    return { type: postConstants.SUBMIT_REQUEST, post };
  }
  function success(post) {
    return { type: postConstants.SUBMIT_SUCCESS, post };
  }
  function failure(error) {
    return { type: postConstants.SUBMIT_FAILURE, error };
  }
}

function getAll() {
  return dispatch => {

    dispatch(request(null));

    const requestOptions = {
      method: "GET",
      headers: { 
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': getToken()
      }
    };
  
    fetch(
         
      setHostURL()+"/apiv1/posts/",
      requestOptions
  
    ).then(response => response.json().then(posts => {
      dispatch(success(posts.result));
    })).catch(err => {
      console.error(err);
    })
  };

  function request() {
    return { type: postConstants.GET_POSTS_REQUEST };
  }
  function success(posts) {
    return { type: postConstants.GET_POSTS_SUCCESS, posts };
  }
  function failure(error) {
    return { type: postConstants.GET_POSTS_FALIURE, error };
  }
}

function detail(post) {
  const id = post._id;
  return dispatch => {

    dispatch(detail(post));

    history.push('/newPost');

  }
  function detail(post) {
    return { type: postConstants.GO_TO_DETAIL, post}
  }
}


function _delete({_id}) {
  const id = _id
  return dispatch => {

    dispatch(request(null));

    const requestOptions = {
      method: "DELETE",
      headers: { 
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': getToken(),
      },
      body: JSON.stringify({id: id})
    };
  
    fetch(
         
      setHostURL()+"/apiv1/posts/",
      requestOptions
  
    ).then(response => response.json().then(() => {
      dispatch(success());
      history.push('/posts');
    })).catch(err => {
      console.error(err);
    })
  };

  function request() {
    return { type: postConstants.DELETE_POSTS_REQUEST };
  }
  function success(posts) {
    return { type: postConstants.DELETE_POSTS_SUCCESS };
  }
  function failure(error) {
    return { type: postConstants.DELETE_POSTS_FALIURE };
  }
};
